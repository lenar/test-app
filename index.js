/**
 *
 * @author rnel <arnacmj@gmail.com>
 * noob
 */


 var app = require('express')();
 var server = require('http').Server(app);

 var io = require('socket.io')(server);

 server.listen(3000);

 app.get('/', function(request, response) {
 	response.sendFile(__dirname + '/index.html');
 });


 io.on('connection', function(socket) {
 	socket.on('add-todo', function(todo) {
 		io.emit('add-todo', todo);
 	});

 	socket.on('remove-todo', function(todo) {
 		io.emit('remove-todo', todo);
 	});

 	socket.on('update-todo', function(todo) {
 		io.emit('update-todo', todo);
 	});
 });